#ifndef BMPTF_H
#define BMPTF_H
#include "image.h"
#include <stdio.h>

/*  deserializer   */
enum readStatus  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_DATA,
  READ_INVALID_HEADER
  /* коды других ошибок  */
  };

enum readStatus fromBmp( FILE* in, struct image* img );

enum  writeStatus  {
  WRITE_OK = 0,
  WRITE_ERROR
};

enum writeStatus toBmp( FILE* out, const struct image * img );
#endif
