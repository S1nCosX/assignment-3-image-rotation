#ifndef FILEIO_H
#define FILEIO_H
#include <stdbool.h>
#include <stdio.h>

enum openStatus  {  
  OPEN_OK = 0,
  OPEN_ERROR
  };

enum openStatus open( FILE** file, const char* path, const bool isReadable);

enum  closeStatus  {
  CLOSE_OK = 0,
  CLOSE_ERROR
};

enum closeStatus close( FILE** file);
#endif
