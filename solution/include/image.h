#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct __attribute__((packed)) pixel { 
    uint8_t b, g, r; 
};

int destroyImage( struct image* image);

struct image createImage(const uint64_t width, const uint64_t height);


void rotate(struct image* source);
#endif
