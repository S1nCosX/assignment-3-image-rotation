#include "../../include/fileIO.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

enum openStatus open( FILE** file, const char* path, const bool isReadable){
    
    *file = fopen(path, (isReadable ? "rb" : "wb"));
    
    if (*file == NULL)
        return OPEN_ERROR;
    return OPEN_OK;
}

enum closeStatus close( FILE** file){
    uint8_t status = fclose(*file);
    if (status == 0)
        return CLOSE_OK;
    return CLOSE_ERROR;
}
