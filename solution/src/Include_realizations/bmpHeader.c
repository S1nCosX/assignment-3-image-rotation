#include "../../include/bmpHeader.h"
#include "../../include/image.h"

struct bmpHeader createBMPHeader(const struct image* image){
    return (struct bmpHeader) {
        //All info from https://learn.microsoft.com/en-us/previous-versions/windows/embedded/aa452883(v=msdn.10)
        .bfType = 0x4d42,   //Specifies the file type. 
                            //It must be set to the signature word BM (0x4D42) to indicate bitmap.
        .bfileSize = (image -> width * sizeof(struct pixel) + (4 - (image ->width) % 4) % 4/*calculating padding*/) 
        * (image ->width) + sizeof(struct bmpHeader),
        .bfReserved = 0, // Reserved; set to zero
        .bOffBits = sizeof(struct bmpHeader),  //Specifies the offset, in bytes,
                                                // from the BITMAPFILEHEADER structure to the bitmap bits
        .biSize = 40,
        .biWidth = image -> width,
        .biHeight = image -> height,
        .biPlanes = 1, //This value must be set to 1.
        .biBitCount = sizeof(struct pixel) * 8, //Specifies the number of bits per pixel.
        .biCompression = 0, //our picture is uncompressed
        .biSizeImage = (image -> width * sizeof(struct pixel) + (4 - (image ->width) % 4) % 4) 
        * (image ->width) 
        };
}
