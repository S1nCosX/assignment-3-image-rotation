#include "../../include/bmpTF.h"
#include "../../include/bmpHeader.h"
#include "../../include/image.h"
#include  <stdint.h>
#include  <stdlib.h>

int64_t getPadding(const struct image* img){
    return (int64_t) (16 - (img->width % 4) * sizeof(struct pixel)) % 4;
}

enum readStatus fromBmp( FILE* in, struct image* img ){
    struct bmpHeader* header = (struct bmpHeader*) malloc(sizeof(struct bmpHeader));
    
    if (!fread(header, sizeof(struct bmpHeader), 1, in)){
        free(header);
        return READ_INVALID_HEADER;
    }

    if (header -> bfType != 0x4d42){
        free(header);
        return READ_INVALID_SIGNATURE;
    }

    if (header->biBitCount != 24){
        free(header);   
        return READ_INVALID_BITS;
    }

    *img = createImage(header->biWidth, header->biHeight);
    free(header);

    int64_t padding = getPadding(img);

    for (int i = 0; i < img->height; i++){
        if(!fread(img->data + img->width * i, sizeof(struct pixel) * img -> width, 1, in))
            return READ_INVALID_DATA;
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum writeStatus toBmp( FILE* out, const struct image * img ){
    struct bmpHeader header = createBMPHeader(img);

    fwrite(&header, sizeof(header), 1, out);

    int64_t padding = getPadding(img);

    int8_t* paddingData = (int8_t*) malloc(padding);
    for(int i = 0; i < padding; i++){
        paddingData[i] = 0;
    }   

    for(int i = 0; i < img->height; i++){
        if(!fwrite(img->data + i*img->width, sizeof(struct pixel), img->width, out)){
            free(paddingData);
            return WRITE_ERROR;
        }
        if(!fwrite(paddingData, padding, 1, out)){
            free(paddingData);
            return WRITE_ERROR;
        }
    }
    
    free(paddingData);
    return WRITE_OK;
}

