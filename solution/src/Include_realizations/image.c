#include "../../include/image.h"
#include  <stdint.h>
#include  <stdlib.h>

struct image createImage(const uint64_t width, const uint64_t height){
    return (struct image) {
        .width = width,
        .height = height,
        .data = (struct pixel*) malloc(sizeof(struct pixel)*width*height)
    };
}

int destroyImage( struct image* image){
    free(image->data);
    return 0;
}

void rotate(struct image* source){
    struct image output = createImage(source->height, source->width);
    
    for (int i= 0; i < source->height; i++){
        for (int j = 0; j <source->width; j++   )
            output.data[j * source->height + (source->height - i - 1)] = source->data[i * source->width + j];
    }
    destroyImage(source);
    *source = output;
}
