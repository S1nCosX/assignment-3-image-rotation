#include "../include/bmpTF.h"
#include "../include/bmpHeader.h"
#include "../include/fileIO.h"
#include "../include/image.h"
#include <stdbool.h>
#include <stdio.h>

int main( int argc, char** argv ) {
    
    FILE* inFile=NULL;
    
    FILE* outFile=NULL;
    if(argc == 3){
      open(&inFile, argv[1], true);
      open(&outFile, argv[2], false);
    }
    
    struct image img = {0,0,NULL};

    fromBmp(inFile, &img);

    rotate(&img);
        
    toBmp(outFile, &img);

    close(&inFile);
    close(&outFile);

    destroyImage(&img);
    return 0;
}
